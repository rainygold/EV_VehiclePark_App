# EV_VehiclePark_App
A C# WPF application that relies on random number generation to create 'Vehicles' and 'Passengers' to pseudo-simulate a parking garage in a commercial setting. More features were desired at inception but this was the first attempt at using the language/framework. There are a few methods that were partially implemented but not used in the current build, and these should be appropriately marked.

Navigate to '\EV_Pres_Proj\EV_Pres_Proj\bin\Release\app.publish' to find the working executable. 

Author: Charlie Murphy 2017
