﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using EV_Pres_Proj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV_Pres_Proj.Tests
{
    [TestClass()]
    public class SimTests
    {
        [TestMethod()]
        public void begin_simTest()
        {
            // Arrange
            Sim two_was_the_best_game = new Sim();
            Garage four_is_a_joke = new Garage();

            // Act
            two_was_the_best_game.begin_sim(four_is_a_joke);

            // Assert
            Assert.IsTrue(two_was_the_best_game.time.Enabled == true);
        }
    }
}