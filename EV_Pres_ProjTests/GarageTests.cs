﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using EV_Pres_Proj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV_Pres_Proj.Tests
{
    [TestClass()]
    public class GarageTests
    {
        [TestMethod()]
        public void check_occupantsTest()
        {
            // Arrange
            Garage gar = new Garage();

            // Act
            gar.fix_size();

            // Assert
            Assert.IsNotNull(gar.check_occupants());
        }

        [TestMethod()]
        public void fix_sizeTest()
        {
            // Arrange
            Garage gar = new Garage();

            // Act
            gar.fix_size();

            // Assert
            Assert.IsNotNull(gar.spaces);
            Assert.IsTrue(gar.checker == 10);
            Assert.IsNotNull(gar.vehicle_brand);
        }

        [TestMethod()]
        public void grid_update_via_listTest()
        {
            // Arrange
            Garage gar = new Garage();

            // Act
            gar.fix_size();
            gar.grid_update_via_list(0);

            // Assert
            Assert.IsTrue(gar.spaces[0].Count > 0);
        }

        [TestMethod()]
        public void add_vehicleTest()
        {
            // Arrange
            Garage gar = new Garage();
            Vehicle veh = new Vehicle();

            // Act & Assert
            Assert.ReferenceEquals(gar.add_vehicle(), veh);


        }

        [TestMethod()]
        public void multi_vehicleTest()
        {
            // Arrange
            Garage gar = new Garage();
            List<Vehicle> grand = new List<Vehicle>();

            // Act
            grand = gar.multi_vehicle();

            // Assert
            Assert.IsTrue(grand.Count > 0);
        }
    }
}