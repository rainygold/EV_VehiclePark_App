﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using EV_Pres_Proj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV_Pres_Proj.Tests
{
    [TestClass()]
    public class PersonTests: Person
    {
        [TestMethod()]
        public void set_valuesTest()
        {
            // Arrange
            PersonTests nancy = new PersonTests();

            // Act
            nancy.set_values(2);

            // Assert
            const int expected_budget = 20;
            Assert.IsTrue(nancy.budget > expected_budget);
            Assert.IsNotNull(nancy.name);

        }

        [TestMethod()]
        public void id_selectTest()
        {
            // Arrange
            PersonTests phil = new PersonTests();

            // Act
            phil.id_select();

            // Assert
            Assert.IsTrue(Enumerable.Range(0, 5).Contains(phil.id));
        }

        [TestMethod()]
        public void name_selectTest()
        {
            // Arrange
            PersonTests jane = new Tests.PersonTests();

            // Act
            jane.set_values(3);


            // Assert
            
            string[] names = new string[] { "George", "John", "Chaz", "Dorothy", "Linda", "Margaret", "Jacque", "Jake", "Penelope", "Peter", "Amy", "Terry", "Harry", "Holt", "Keith", "Gina", "Pilsner" };
            Assert.IsTrue((names.Any(jane.name.Contains)));
        }

        [TestMethod()]
        public void budget_subTest()
        {
            // Arrange
            PersonTests hopper = new Tests.PersonTests();

            // Act
            hopper.set_values(3);
            int original_budget = hopper.budget;
            int new_budget = hopper.budget_sub();

            // Assert
            Assert.IsTrue(new_budget < original_budget);
        }
    }
}