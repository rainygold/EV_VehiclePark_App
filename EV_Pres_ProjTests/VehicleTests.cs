﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using EV_Pres_Proj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV_Pres_Proj.Tests
{
    [TestClass()]
    public class VehicleTests
    {
        [TestMethod()]
        public void grab_combined_budgetTest()
        {

            // Arrange
            Taxi tax = new Taxi();

            // Act
            tax.taxi_id();
            tax.generate_driver();
            tax.combine_all_budgets();

            // Assert
            Assert.IsNotNull(tax.grab_combined_budget());
        }

        [TestMethod()]
        public void generate_driverTest()
        {
            // Arrange
            Car car = new Car();

            // Act
            car.car_id();
            car.generate_driver();

            // Assert
            Assert.IsTrue(car.occupants.Count > 0);
            Assert.IsTrue(car.occupants.Count == car.seats);
        }
    }
}