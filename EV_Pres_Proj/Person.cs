﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV_Pres_Proj
{
     public class Person
    {

        // declared variables w/ gets-sets
        public int budget { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public static Random random = new Random();

        // person method for setting budget value
        protected void set_values(int switchid)
        {
            switch (switchid)
            {
                case 1:
                    budget = random.Next(1, 10);
                    name = name_select();
                    break;

                case 2:
                    budget = random.Next(20, 40);
                    name = name_select();
                    break;

                case 3:
                    budget = random.Next(50, 80);
                    name = name_select();
                    break;

                case 4:
                    budget = random.Next(90, 100);
                    name = name_select();
                    break;
            }
        }

        // random name selector for new people
        protected string name_select()
        {
            string[] names = new string[] { "George", "John", "Chaz", "Dorothy", "Linda", "Margaret", "Jacque", "Jake", "Penelope", "Peter", "Amy", "Terry", "Harry", "Holt", "Keith", "Gina", "Pilsner" };
            string[] last_names = new string[] { "Murphy", "Smith", "Gomez", "Peralta", "Santiago", "Nilsen", "Winehouse", "Denim", "Lacoste", "Miller", "Potter", "Granger", "Dumbledore", "Grint" };

            int name_bingo = random.Next(0, names.Length);
            int last_names_bingo = random.Next(0, last_names.Length);
            return names[name_bingo] + " " + last_names[last_names_bingo];
        }

        // random id assignment for new people
        protected int id_select()
        {
            int[] ids = new int[] { 1, 2, 3, 4 };
            int id_bingo = random.Next(0, 5);
            return ids[id_bingo];
        }


        // interval budget decrease
        protected int budget_sub()
        {
            int subval = random.Next(1, 15);
            return budget - subval;

        }


    }  // end class Person


    // derived classes Person
    class Child : Person
    {

        // sets unique id no. for each derived
        public void child_id()
        {
            id = 1;
            set_values(id);
        }

    }

    class Teenager : Person
    {

        public void teen_id()
        {
            id = 2;
            set_values(id);
        }

    }

    class Adult : Person
    {
        public void adult_id()
        {
            id = 3;
            set_values(id);
        }

    }

    class Elderly : Person
    {

        public void elder_id()
        {
            id = 4;
            set_values(id);
        }

    }

}

