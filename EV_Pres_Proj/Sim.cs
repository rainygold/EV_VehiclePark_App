﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace EV_Pres_Proj
{
    public class Sim
    {

        // class variables
        public TimeSpan counter;
        public TimeSpan increase;
        public Timer time;
        public int grid;

        // sets time to default value - reset
        protected void reset_time()
        {
            counter = new TimeSpan(8, 0, 0);
            grid = 0;
        }

        // starts the simulation
        public void begin_sim(Garage main_park)
        {
            reset_time();
            time = new Timer(1000);
            time.Enabled = true;
            main_park.fix_size();
            time.Elapsed += delegate { half_interval(main_park); };
            time.Start();
        }


        // time interval - 5 seconds = 30 mins
        protected void half_interval(Garage main_park)
        {
            increase = new TimeSpan(0, 30, 0);


            // code will break if time limit is reached
            if (counter.Hours < 20)
            {
                // add to timespan
                counter = counter.Add(increase);
                Console.WriteLine(counter);
                // add to grid
                main_park.grid_update_via_list(grid);
                // check if current list is full, if so then change to new list
                if (main_park.spaces[grid].Count() == 10)
                {
                    grid++;
                }

            }
            else
            {
                // when timelimit is reached then break!
                return;
            }

        }

    }
}
