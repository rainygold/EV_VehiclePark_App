﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EV_Pres_Proj
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public TimeSpan window_counter;
        public TimeSpan window_increase;
        public Timer window_time;
        public static int motorcycle_counter = 0;
        public static int car_counter = 0;
        public static int suv_counter = 0;
        public static int taxi_counter = 0;
        public static int fare_total = 0;

        public MainWindow()
        {

            InitializeComponent();
        }

        // TODO!

        // ADD METHOD TO CREATE TOTAL BUDGET OF ALL PASSENGERS AND DEDUCT FROM BUDGET (partially implemented, not completed)

        // Begin Simulation button events
        private void button_Click(object sender, RoutedEventArgs e)
        {
            // objects to call methods on
            start_sim_button.IsEnabled = false;
            Sim sim = new Sim();
            Garage gar = new Garage();

            // initiates simulation using new garage
            sim.begin_sim(gar);

            System.Windows.MessageBox.Show("Simulation has started.");

            // data source for grid
            lst.ItemsSource = gar.vehicle_brand;
            source_begin(lst, gar);
            lst.Items.Refresh();

        }

        // adds motorcycle passenger's names to textbox
        private void cycle_people(Garage value)
        {

            // checks for lists in a garage
            foreach (List<Vehicle> dope in value.spaces.ToArray())
            {

                // checks for vehicles in the lists
                foreach (Vehicle veh in dope.ToArray())
                {

                    // append to appropriate box
                    switch (veh.brand)
                    {
                        case "Motorcycle":
                            motorcycle_counter++;
                            foreach (Person per in veh.occupants)
                            {
                                cyclePersonBox.AppendText("\n" + per.name);
                                veh.brand = "Motorcycle1";
                                cycleCounter.Clear();
                                cycleCounter.AppendText(motorcycle_counter.ToString());
                                fare_total += veh.fare;
                            }
                            break;

                        case "Car":
                            car_counter++;
                            foreach (Person per in veh.occupants)
                            {
                                carPersonBox.AppendText("\n" + per.name);
                                veh.brand = "Car1";
                                carCounter.Clear();
                                carCounter.AppendText(car_counter.ToString());
                                fare_total += veh.fare;
                            }
                            break;

                        case "SUV":
                            suv_counter++;
                            foreach (Person per in veh.occupants)
                            {
                                suvPersonBox.AppendText("\n" + per.name);
                                veh.brand = "SUV1";
                                suvCounter.Clear();
                                suvCounter.AppendText(suv_counter.ToString());
                                fare_total += veh.fare;
                            }
                            break;

                        case "Taxi":
                            taxi_counter++;
                            foreach (Person per in veh.occupants)
                            {
                                taxiPersonBox.AppendText("\n" + per.name);
                                veh.brand = "Taxi1";
                                taxiCounter.Clear();
                                taxiCounter.AppendText(taxi_counter.ToString());
                                fare_total += veh.fare;
                            }
                            break;


                    }
                }
            }

        }

        // runs through all vehicles in the garage and checks their budgets/deducts from them
        private void check_vehicle_budgets(Garage value)
        {

            foreach (List<Vehicle> list in value.spaces.ToArray())
            {
                foreach (Vehicle veh in list.ToArray())
                {
                    if (veh.grab_combined_budget() > 0)
                    {
                        veh.deduct_from_combined_budget();
                        Console.WriteLine(veh.grab_combined_budget().ToString());
                        
                    }

                    // if budget is below 0 then the vehicle 'leaves' the garage
                    if (veh.grab_combined_budget() <= 0)
                    {
                        switch (veh.brand)
                        {


                            case "Motorcycle1":
                                cycleCounter.Clear();
                                motorcycle_counter--;
                                list.Remove(veh);
                                break;

                            case "Car1":
                                car_counter--;
                                list.Remove(veh);
                                break;

                            case "SUV1":
                                suv_counter--;
                                list.Remove(veh);
                                break;

                            case "Taxi1":
                                suv_counter--;
                                list.Remove(veh);
                                break;

                              
                        }

                    }
                }
            }
        }

        // initiates update timer
        private void source_begin(ItemsControl example, Garage value)
        {
            window_time = new Timer(1000);
            window_time.Enabled = true;

            window_time.Elapsed += delegate { begin_the_timer(example, value); };
            window_time.Start();

        }

        // starts the timer for update process
        private void begin_the_timer(ItemsControl example, Garage value)
        {
            this.Dispatcher.Invoke(() =>
            {
                cycle_people(value);
                // check_vehicle_budgets(value);
                fareBoxCounter.Clear();
                fareBoxCounter.AppendText("$" + fare_total.ToString());
                example.Items.Refresh();
            });
        }

        // resets grid cycle
        private void reset_window_time()
        {
            window_counter = new TimeSpan(8, 0, 0);
        }

        private void reset_button_Click(object sender, RoutedEventArgs e)
        {
            
            start_sim_button.IsEnabled = true;
            Sim sim = new Sim();
            Garage gar = new Garage();

            System.Windows.MessageBox.Show("Simulation has reset.");

            // resets all GUI variables
            lst.ItemsSource = gar.vehicle_brand;
            lst.Items.Refresh();
            carCounter.Clear();
            cycleCounter.Clear();
            suvCounter.Clear();
            taxiCounter.Clear();
            cyclePersonBox.Clear();
            carPersonBox.Clear();
            suvPersonBox.Clear();
            taxiPersonBox.Clear();
            fareBoxCounter.Clear();
            fare_total = 0;
            motorcycle_counter = 0;
            car_counter = 0;
            suv_counter = 0;
            taxi_counter = 0;
        }
    }
}
