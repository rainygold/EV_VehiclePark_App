﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV_Pres_Proj
{
    public class Vehicle

    {
        // declared variables w/ gets-sets
        public string brand { get; set; }
        public int id { get; set; }
        public int seats { get; set; }
        public int time_depart { get; set; }
        public List<Person> occupants { get; set; }
        public static Random random = new Random();
        public static int combined_budget { get; set; }
        public int fare { get; set; }

        // called methods
        protected void seat_assign(int seat_id)
        {
            switch (id)
            {
                case 1:
                    seats = 1;
                    brand = "Motorcycle";
                    fare = 3;
                    combined_budget = 0;
                    occupants = new List<Person>();
                    break;

                case 2:
                    seats = 2;
                    brand = "Car";
                    fare = 5;
                    combined_budget = 0;
                    occupants = new List<Person>();
                    break;

                case 3:
                    seats = 4;
                    brand = "SUV";
                    fare = 10;
                    combined_budget = 0;
                    occupants = new List<Person>();
                    break;

                case 4:
                    seats = 6;
                    brand = "Taxi";
                    fare = 15;
                    combined_budget = 0;
                    occupants = new List<Person>();
                    break;
            }
        }

        // random id allocation for new vehicles
        protected void setID()
        {
            id = random.Next(0, 5);
        }

        public int grab_combined_budget()
        {
            return combined_budget;
        }

        // combines the budgets of all passengers
        public void combine_all_budgets()
        {
            foreach (Person per in occupants)
            {
                per.budget += grab_combined_budget();
                //Console.Write("/"+ veh.combined_budget);
            }
        }

        // interval reduction of combined budget variable
        public int deduct_from_combined_budget()
        {
            int deduct = random.Next(0, 15);
            return combined_budget - deduct;
        }

        // generates Person(s) for the vehicle
        public void generate_driver()
        {
            int original_value = seats;

            // runs code until vehicle is filled
            for (int counter = 0; counter < original_value; counter++)
            {
                Random trial = new Random();
                int result;
                result = trial.Next(1, 4);
                switch (result)
                {
                    case 1:
                        Child newchild = new Child();
                        newchild.child_id();
                        for (int runs = 0; runs < original_value; runs++)
                        {
                            if (runs < original_value)
                            {
                                occupants.Add(newchild);
                                break;
                            }

                        }
                        break;


                    case 2:
                        Teenager newteen = new Teenager();
                        newteen.teen_id();
                        for (int runs = 0; runs < original_value; runs++)
                        {
                            if (runs < original_value)
                            {
                                occupants.Add(newteen);

                                break;
                            }
                        }
                        break;

                    case 3:
                        Adult newadult = new Adult();
                        newadult.adult_id();
                        for (int runs = 0; runs < original_value; runs++)
                        {
                            if (runs < original_value)
                            {
                                occupants.Add(newadult);
                                break;
                            }
                        }
                        break;

                    case 4:
                        Elderly newelder = new Elderly();
                        newelder.elder_id();
                        for (int runs = 0; runs < original_value; runs++)
                        {
                            if (runs < original_value)
                            {
                                occupants.Add(newelder);
                                break;
                            }
                        }
                        break;
                }
            }
        }

    }


    // derived classes Vehicle
    public class Motorcycle : Vehicle
    {

        // sets unique id no. for each derived
        public void cycle_id()
        {
            id = 1;
            seat_assign(id);
        }

    }

    public class Car : Vehicle
    {

        public void car_id()
        {
            id = 2;
            seat_assign(id);
        }

    }

    public class Suv : Vehicle
    {
        public void suv_id()
        {
            id = 3;
            seat_assign(id);
        }

    }

    public class Taxi : Vehicle
    {

        public void taxi_id()
        {
            id = 4;
            seat_assign(id);
        }

    }



}
