﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV_Pres_Proj
{
    public class Garage
    {
        // declared class variables
        public List<List<Vehicle>> spaces { get; set; }
        public List<List<string>> vehicle_brand { get; set; }
        protected int occupancy { get; set; }
        public int result;
        public int checker;

        // returns int with array size of garage
        public int check_occupants()
        {
            return occupancy = spaces.ToArray().Length;
        }

        // assigns variable for grid material
        public void fix_size()
        {
            spaces = new List<List<Vehicle>>();
            vehicle_brand = new List<List<string>>();
            checker = 10;
        }


        // new method for adding to grid
        public void grid_update_via_list(int grid)
        {

            {

                if (spaces != null && vehicle_brand != null)
                {

                    // initial set up for list
                    if (spaces.Count() < 1)
                    {
                        Console.WriteLine("Initializing list...");

                        spaces.Add(new List<Vehicle>());
                        vehicle_brand.Add(new List<string>());

                        for (int i = 0; i < multi_vehicle().Count; i++)
                        {
                            if (spaces[grid].Count() < 10)
                            {
                                spaces[grid].Add(multi_vehicle()[i]);
                                vehicle_brand[grid].Add(multi_vehicle()[i].brand);
                                
                            }
                        }
                    }

                    // if there's more than one list & the list in iteration isn't full then add a vehicle
                    if (spaces[grid].Count() < checker)
                    {
                        Console.Write("Adding to lists...");

                        for (int i = 0; i < multi_vehicle().Count; i++)
                        {
                            if (spaces[grid].Count() < 10)
                            {
                                spaces[grid].Add(multi_vehicle()[i]);
                                vehicle_brand[grid].Add(multi_vehicle()[i].brand);

                            }
                        }
                    }
                    // if list is full, make a new one
                    if (spaces[grid].Count() == checker)
                    {
                        spaces.Add(new List<Vehicle>());
                        vehicle_brand.Add(new List<string>());
                        spaces[grid + 1].Add(add_vehicle());
                        vehicle_brand[grid + 1].Add(add_vehicle().brand);
                    }

                }
                else
                {
                    Console.Error.WriteLine("Garage is null!");
                }
            }
        }

        // creates a random vehicle derivative to populate the parking lot
        public Vehicle add_vehicle()
        {
            Random trial = new Random();
            result = trial.Next(1, 5);
            switch (result)
            {
                case 1:
                    Motorcycle cycle = new Motorcycle();
                    cycle.cycle_id();
                    cycle.generate_driver();
                    cycle.combine_all_budgets();
                    return cycle;
                case 2:
                    Car car = new Car();
                    car.car_id();
                    car.generate_driver();
                    car.combine_all_budgets();
                    return car;

                case 3:
                    Suv suv = new Suv();
                    suv.suv_id();
                    suv.generate_driver();
                    suv.combine_all_budgets();
                    return suv;

                case 4:
                    Taxi taxi = new Taxi();
                    taxi.taxi_id();
                    taxi.generate_driver();
                    taxi.combine_all_budgets();
                    return taxi;

                default:
                    Vehicle default_vehicle = new Vehicle();
                    return default_vehicle;
            }

        }

        // returns multiple vehicle brand strings
        public List<Vehicle> multi_vehicle()
        {
            Random randy = new Random();
            List<Vehicle> collection = new List<Vehicle>();
            int no_of_vehicles = randy.Next(1, 4);

            for (int i = 0; i < no_of_vehicles; i++)
            {
                collection.Add(add_vehicle());
            }
            return collection;
        }

    }
}
